import React from "react";
import MealsSummery from "./MealsSummery";
import AvaliableMeals from "./AvaliableMeals";

export default function Meals() {
  return (
    <>
      <MealsSummery />
      <AvaliableMeals />
    </>
  );
}
