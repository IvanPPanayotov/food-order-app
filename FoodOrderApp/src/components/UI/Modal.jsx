// Modal.js
import React, { useState } from "react";
import "./Modal.css";
const Modal = ({ isOpen, onClose, children }) => {
  if (!isOpen) return null;

  const handleModalContentClick = (e) => {
    e.stopPropagation();
  };

  return (
    <div className="modal-overlay" onClick={onClose}>
      <div className="modal" onClick={handleModalContentClick}>
        <div className="modal-content">{children}</div>
      </div>
    </div>
  );
};

export default Modal;
