import React from "react";
import "./input.css";

export default function Input({ input, label }) {
  return (
    <div className={`input`}>
      <label htmlFor={input.id}>{label}</label>
      <input {...input} />
    </div>
  );
}
