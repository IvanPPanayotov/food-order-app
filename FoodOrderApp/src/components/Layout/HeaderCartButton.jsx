import React, { useState } from "react";

import "./HeaderCartButton.css";
import CartIcon from "../Cart/CartIcon";
import Modal from "../UI/Modal";
export default function HeaderCartButton() {
  const [isModalOpen, setModal] = useState(false);
  const closeModal = () => {
    setModal(!isModalOpen);
  };
  return (
    <>
      <button className={`button`} onClick={() => closeModal()}>
        <span className={`icon`}>
          <CartIcon />
        </span>
        <span>Your Cart</span>
        <span className={`badge`}>3</span>
      </button>
      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <h1>Hello</h1>
      </Modal>
    </>
  );
}
